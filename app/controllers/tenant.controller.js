const Tenant = require("../models/tenant.model.js");

// Create and Save a new tenant
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    // Create a tenant
    const tenant = new Tenant({
        countryId:  req.body.countryId,
        languageId: req.body.languageId,
        contactId: req.body.contactId,
        name: req.body.name,
        address: req.body.address,
        zipCode: req.body.zipCode,
        city: req.body.city,
        vatNumber: req.body.vatNumber,
        kvkNumber: req.body.kvkNumber,
        website: req.body.website,
        logo: req.body.logo,
        bankAccount: req.body.bankAccount,
        domain: req.body.domain,
        useGrayScaleInFlex: req.body.useGrayScaleInFlex,
        maxUsers: req.body.maxUsers,
        endDate: req.body.endDate,
    });
  
    // Save tenant in the database
    Tenant.create(tenant, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the tenant."
        });
      else res.send(data);
    });
  };
// Retrieve all tenants from the database.
exports.findAll = (req, res) => {
    Tenant.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tenants."
        });
      else res.send(data);
    });
  };
// Find a single tenant with a tenantId
exports.findOne = (req, res) => {
    Tenant.findById(req.params.tenantId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found tenant with id ${req.params.tenantId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving tenant with id " + req.params.tenantId
          });
        }
      } else res.send(data);
    });
  };

// Update a tenant identified by the tenantId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    Tenant.updateById(
      req.params.tenantId,
      new Tenant(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found tenant with id ${req.params.tenantId}.`
            });
          } else {
            res.status(500).send({
              message: "Error updating tenant with id " + req.params.tenantId
            });
          }
        } else res.send(data);
      }
    );
  };

// Delete a tenant with the specified tenantId in the request
exports.delete = (req, res) => {
    Tenant.remove(req.params.tenantId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found tenant with id ${req.params.tenantId}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete tenant with id " + req.params.tenantId
          });
        }
      } else res.send({ message: `tenant was deleted successfully!` });
    });
  };

// Delete all tenants from the database.
exports.deleteAll = (req, res) => {
    Tenant.removeAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all tenants."
        });
      else res.send({ message: `All tenants were deleted successfully!` });
    });
  };
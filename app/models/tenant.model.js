const sql = require("./db.js");

// constructor
const Tenant = function(Tenant) {
    this.countryId=  Tenant.countryId,
    this.languageId= Tenant.languageId,
    this.contactId= Tenant.contactId,
    this.name= Tenant.name,
    this.address= Tenant.address,
    this.zipCode= Tenant.zipCode,
    this.city= Tenant.city,
    this.vatNumber= Tenant.vatNumber,
    this.kvkNumber= Tenant.kvkNumber,
    this.website= Tenant.website,
    this.logo= Tenant.logo,
    this.bankAccount= Tenant.bankAccount,
    this.domain= Tenant.domain,
    this.useGrayScaleInFlex= Tenant.useGrayScaleInFlex,
    this.maxUsers= Tenant.maxUsers,
    this.endDate= Tenant.endDate
};

Tenant.create = (newTenant, result) => {
  sql.query("INSERT INTO Tenant SET ?", newTenant, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created Tenant: ", { id: res.insertId, ...newTenant });
    result(null, { id: res.insertId, ...newTenant });
  });
};

Tenant.findById = (TenantId, result) => {
  sql.query(`SELECT * FROM Tenant WHERE id = ${TenantId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found Tenant: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Tenant with the id
    result({ kind: "not_found" }, null);
  });
};

Tenant.getAll = result => {
  sql.query("SELECT * FROM Tenant", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Tenants: ", res);
    result(null, res);
  });
};

Tenant.updateById = (id, Tenant, result) => {
//   sql.query(
//     "UPDATE Tenant SET email = ?, name = ?, active = ? WHERE id = ?",
//     [Tenant.email, Tenant.name, Tenant.active, id],
//     (err, res) => {
//       if (err) {
//         console.log("error: ", err);
//         result(null, err);
//         return;
//       }

//       if (res.affectedRows == 0) {
//         // not found Tenant with the id
//         result({ kind: "not_found" }, null);
//         return;
//       }

//       console.log("updated Tenant: ", { id: id, ...Tenant });
//       result(null, { id: id, ...Tenant });
//     }
//   );
};

Tenant.remove = (id, result) => {
  sql.query("DELETE FROM Tenant WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Tenant with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Tenant with id: ", id);
    result(null, res);
  });
};

Tenant.removeAll = result => {
  sql.query("DELETE FROM Tenant", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} Tenants`);
    result(null, res);
  });
};

module.exports = Tenant;
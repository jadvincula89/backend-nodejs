// module.exports = app => {
//   const customers = require("../controllers/customer.controller.js");

//   // Create a new Customer
//   app.post("/customers", customers.create);

//   // Retrieve all Customers
//   app.get("/customers", customers.findAll);

//   // Retrieve a single Customer with customerId
//   app.get("/customers/:customerId", customers.findOne);

//   // Update a Customer with customerId
//   app.put("/customers/:customerId", customers.update);

//   // Delete a Customer with customerId
//   app.delete("/customers/:customerId", customers.delete);

//   // Create a new Customer
//   app.delete("/customers", customers.deleteAll);
  
// };

const customers = require("../controllers/customer.controller.js");
var express = require('express');
var router = express.Router();
router.route('/').post(customers.create)
router.route('/').get(customers.findAll)
router.route('/:customerId').get(customers.findOne)
router.route('/:customerId').put(customers.update)
router.route('/:customerId').delete(customers.delete)
router.route('/').delete(customers.deleteAll)
module.exports = router;


const tenants = require("../controllers/tenant.controller.js");
var express = require('express');
var router = express.Router();
router.route('/').post(tenants.create)
router.route('/').get(tenants.findAll)
router.route('/:tenantId').get(tenants.findOne)
router.route('/:tenantId').put(tenants.update)
router.route('/:tenantId').delete(tenants.delete)
router.route('/').delete(tenants.deleteAll)
module.exports = router;

